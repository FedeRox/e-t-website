.. _workshop:

eT workshops
============

December 2022
-------------

The eT workshop organized from December 12 to December 16, 2022,
at NTNU in Trondheim was focused on preparation for the second major release of eT.

Attending the workshop is free of charge.

Organizers
^^^^^^^^^^
H\. Koch, A\. C\. Paul, E\. F\. Kjønstad, S\. D\. Folkestad


For details
^^^^^^^^^^^
Send email to support@etprogram.org
