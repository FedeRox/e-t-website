
electric field
--------------

Keywords related to the specification of electric field pulses for time-dependent coupled cluster calculations. 

Required keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``envelope: {[integer],[integer],...}``
   
   Specifies the envelopes to use for electric field pulse :math:`1,2,\ldots`.

   Valid keyword values are:
   
   - ``1`` Use Gaussian envelope.  
   - ``2`` Use sine squared envelope.  

.. container:: sphinx-custom

   ``x polarization: {[real], [real], ...}``
   
   :math:`x` polarization for electric field pulse :math:`1,2,\ldots`. 

.. container:: sphinx-custom

   ``y polarization: {[real], [real], ...}``
   
   :math:`y` polarization for electric field pulse :math:`1,2,\ldots`. 

.. container:: sphinx-custom

   ``z polarization: {[real], [real], ...}``
   
   :math:`z` polarization for electric field pulse :math:`1,2,\ldots`. 

.. container:: sphinx-custom

   ``central time: {[real], [real], ...}``
   
    Specifies the central time of electric field pulse :math:`1,2,\ldots`. 

.. container:: sphinx-custom

   ``width: {[real], [real], ...}``
   
    Specifies the temporal widths of electric field pulse :math:`1,2,\ldots` in atomic units. 
    The width corresponds to the Gaussian root-mean-squared width of a pulse with a Gaussian envelope and the period of a pulse with a sine squared envelope.

.. container:: sphinx-custom

   ``central angular frequency: {[real], [real], ...}``
   
    Specifies the central angular frequencies of electric field pulse :math:`1,2,\ldots` in atomic units.

.. container:: sphinx-custom

   ``peak strength: {[real], [real], ...}``
   
    Specifies the amplitude of the carrier wave of electric field pulse :math:`1,2,\ldots` in atomic units.

.. container:: sphinx-custom

   ``phase shift: {[real], [real], ...}``
   
    Specifies the (carrier-envelope) phase shifts of electric field pulse :math:`1,2,\ldots`. The shift corresponds to the difference between the middle of the envelope and the maximum of the carrier wave.


Optional keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``repetition: {[real], [real], ...}``
   
   Default: ``{1,1,...}`` (a single instance of each pulse)

   Number of instances of electric field pulse :math:`1,2,\ldots`. The separation of the repeated pulses can be specified by the ``separation`` keyword.

.. container:: sphinx-custom

   ``separation: {[real], [real], ...}``

   Number of repetitions (copies) of electric field pulse :math:`1,2,\ldots`. 

   .. note::

      Should only be specified if the ``repetition`` keyword has been specified.
