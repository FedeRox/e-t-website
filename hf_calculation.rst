.. _hf-calc:

Hartree-Fock calculation
==========================

If you have not already done so,
you should take a look at the :ref:`eT_launch<eT_launch>` page
to get an understanding of how the launch script works.

Setting up an HF calculation
----------------------------

A Hartree-Fock (HF) calculation is performed by specifying a HF wavefunction 
in the :ref:`method section <method-section>` of the input file. 
Available HF methods are RHF (``hf``), UHF (``uhf``), and MLHF (``mlhf``).
E.g., for a RHF calculation we write

.. code-block:: none

   method
      hf
   end method

At the HF level, single-point calculation and geometry optimization can be performed. 
To select one of them, specify either ``ground state`` or ``ground state geoopt`` in the :ref:`do section <do-section>`. 
E.g., for a single-point calculation 

.. code-block:: none
   
   do
      ground state
   end do

A minimal working example for an RHF single-point calculation on water:

.. code-block:: none

  system
     name: H2O
     charge: 0
  end system

  do
     ground state
  end do

  method
     hf
  end method

  geometry
  basis: cc-pVDZ
  H          0.86681        0.60144        0.00000
  H         -0.86681        0.60144        0.00000
  O          0.00000       -0.07579        0.00000
  end geometry
  
Save this as ``h2o.inp`` and invoke the :ref:`launch<eT_launch>` script.

.. code-block:: none

   path/to/eT_launch.py h2o.inp

After the calculation finished you should find ``h2o.out`` and ``h2o.timing.out`` 
in your working directory. 
If the calculation exited successfully 
(look for ``eT terminated successfully!`` at the bottom of the file), 
the output file should show iteration information 
for the solver converging the Hartree-Fock equations. 
The table with the iteration information should end with something like this:

.. code-block:: none

   ...
   7           -75.989795840960     0.5997E-05     0.2341E-07
   8           -75.989795841698     0.2378E-05     0.7377E-09
   9           -75.989795841773     0.2055E-06     0.7489E-10
   ---------------------------------------------------------------
   Convergence criterion met in 9 iterations!

Including the :ref:`solver scf <scf-section>` section 
in the input allows to specify the settings of the HF calculation, 
as e.g. the energy or gradient thresholds, in more detail.
You may want to loosen the thresholds for the HF calculation 
as the default energy and gradient thresholds (:math:`10^{-7}`) 
are chosen to be rather tight.
If you also want to get the molecular orbital coefficients, 
the ``print orbitals`` keyword has to be specified in this section.

.. code-block:: none 

   solver scf
     energy threshold:   1.0d-4
     residual threshold: 1.0d-4
     print orbitals
   end solver scf
   
The orbital coefficients are written to a separate file which is automatically copied
to the output directory by the :ref:`launch<eT_launch>` script. 
The orbital coefficient file would be called ``h2o.mo_coefficients.out`` in this example.


Relevant input sections
-----------------------

:ref:`Solver SCF <scf-section>`

:ref:`HF mean value <hf-mean-value-section>`
